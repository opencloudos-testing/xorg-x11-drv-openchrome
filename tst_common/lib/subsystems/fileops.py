# -*- coding: utf-8 -*-

"""
@file    : fileops.py
@time    : 2022-11-30 10:56:14
@author  : wenjiachen（陈文嘉）
@version : 1.0
@contact : wenjiachen@tencent.com
@desc    : None
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os


def read(path):
    with open(path) as f:
        return f.read()


def readlines(path):
    with open(path) as f:
        return [c.rstrip('\n') for c in f.readlines()]


def write(path, cont):
    with open(path, 'w') as f:
        return f.write(cont)


def mkdir(path, mode=0o777):
    os.mkdir(path, mode)


def rmdir(path):
    os.rmdir(path)
