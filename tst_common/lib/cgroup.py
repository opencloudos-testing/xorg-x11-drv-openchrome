# -*- coding: utf-8 -*-

"""
@file    : cgroup.py
@time    : 2022-10-24 21:16:20
@author  : wenjiachen（陈文嘉）
@version : 1.0
@contact : wenjiachen@tencent.com
@desc    : None
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


class CGroup(object):
    """
    A CGroup representation of the filesystem tree provided by cgroups.
    """

    def __ini__(self, root_path=b"/sys/fs/cgroup/", groups=None, sub_groups=None):
        pass
