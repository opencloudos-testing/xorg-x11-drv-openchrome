#!/bin/bash
# 测试套管理工具
# 详细信息参考wiki: https://iwiki.woa.com/pages/viewpage.action?pageId=1513482298

export LANG=en_US.UTF-8
export LANGUAGE="en_US:en"
export LC_ALL="en_US.UTF-8"

if [ -d "$TST_TS_TOPDIR/tst_common" ]; then
    source "$TST_TS_TOPDIR/tst_common/lib/common.sh" || exit 1
    source "$TST_TS_TOPDIR/tst_common/lib/signature.sh" || exit 1
    source "$TST_TS_TOPDIR/tst_common/lib/tsuite_stress.sh" || exit 1
    export TST_COMMON_TOPDIR="$TST_TS_TOPDIR/tst_common"
elif [ -d "$TST_TS_TOPDIR/lib" ]; then
    # 只是tst_suite_common使用
    source "$TST_TS_TOPDIR/lib/common.sh" || exit 1
    source "$TST_TS_TOPDIR/lib/signature.sh" || exit 1
    source "$TST_TS_TOPDIR/lib/tsuite_stress.sh" || exit 1
    export TST_COMMON_TOPDIR="$TST_TS_TOPDIR"
else
    echo "can't find the tsuite common file"
    exit 1
fi

[ -z "$TST_TS_SYSDIR" ] && export TST_TS_SYSDIR="$TST_TS_TOPDIR/logs/.ts.sysdir"

tsuite_usage() {
    echo -e ""
    echo -e "./tsuite options sub_command sub_options"
    echo -e "    help: 显示帮助信息"
    echo -e "    new sh|c|py case_name [template]: 新增测试用例"
    echo -e "        sh|c|py: 【必选】三选一，sh表示Shell脚本用例，c表示C用例，py表示Python脚本用例"
    echo -e "        case_name: 【必选】要创建的用例名，同时用作文件名"
    echo -e "        template: 【可选】不使用默认用例模板时，可以指定一个文件用作新用例模板"
    echo -e "    list: 列出本测试套的测试用例"
    echo -e "    config: 编译前配置测试套"
    echo -e "    compile: 编译测试套"
    echo -e "    setup: 执行测试套setup"
    echo -e "    run options: 执行测试用例，options可选参数如下："
    echo -e "        null: 不指定时默认执行所有用例"
    echo -e "        case_path: 执行指定用例"
    echo -e "        -l level: 执行指定级别的用例，例如：0,1,2"
    echo -e "        -f case_list: 执行指定列表文件的用例"
    echo -e "    stress cfg-file: 执行cfg-file文件指定的压力测试"
    echo -e "    teardown: 执行测试套teardown"
    echo -e "    clean: 执行make clean"
    echo -e "    cleanall: 执行make cleanall，在clean基础上删除所有临时文件等"
    echo -e "    export: 检查用例信息并导出到json或excel"
    echo -e "更多信息参考wiki: https://iwiki.woa.com/p/1513482298"
    echo -e ""
}

get_all_testcase() {
    local _tst_case_dir=./testcase
    if [ ! -d "$_tst_case_dir" ]; then
        echo "no testcase dir"
        return 1
    fi
    touch "$TST_TS_SYSDIR/all_testcase.list"
    grep -r "@用例名称:" $_tst_case_dir | awk -F : '{print $1}' | sort | uniq >"$TST_TS_SYSDIR/all_testcase.list"
}

tsuite_list() {
    local _tst_index=1

    get_all_testcase
    local _tst_nr_all_case
    _tst_nr_all_case=$(wc -l <"$TST_TS_SYSDIR/all_testcase.list")

    echo ""
    for i in $(seq "$_tst_nr_all_case"); do
        local _tst_case_file
        local _tst_case_name
        local _tst_case_auto
        _tst_case_file="$(sed -n "${i}p" "$TST_TS_SYSDIR/all_testcase.list")"
        _tst_case_name=$(get_case_attr "用例名称" "$_tst_case_file")
        _tst_case_level=$(get_case_attr "用例级别" "$_tst_case_file")
        _tst_case_auto=$(get_case_attr "自动化" "$_tst_case_file")
        if [ "$_tst_case_auto" == 0 ]; then
            _tst_case_auto="$(print_purple manual)"
        else
            _tst_case_auto="$(print_green auto)  "
        fi
        printf "%4d : %-25s %d %s %s\n" $_tst_index "$_tst_case_name" "$_tst_case_level" "$_tst_case_auto" "$_tst_case_file"
        _tst_index=$((_tst_index + 1))
    done
    echo ""
    echo "total $_tst_nr_all_case testcase"
    echo ""
}

# 执行单个用例
# $1 -- 用例文件，Shell和Python用例直接执行脚本文件，C可以是源码文件，也可以是二进制文件（两个文件要同名，后缀不同）
tsuite_run_one() {
    local _tst_input_file="$1"
    shift

    local _tst_ret=0
    local _tst_log_dir="$TST_TS_TOPDIR/logs/testcase"

    local _tst_exec_file
    local _tst_case_file
    # C用例
    if file "$_tst_input_file" | grep -w "ELF" >/dev/null; then
        _tst_exec_file="$_tst_input_file"
        _tst_case_file="${_tst_exec_file%.test}"
        _tst_case_file="${_tst_case_file}.c"
    elif file "$_tst_input_file" | grep -w "C source" >/dev/null; then
        _tst_exec_file="${_tst_input_file%.c}"
        _tst_exec_file="${_tst_exec_file}.test"
        _tst_case_file="${_tst_input_file}"
    else
        _tst_exec_file="${_tst_input_file}"
        _tst_case_file="${_tst_input_file}"
    fi
    local _tst_case_name
    _tst_case_name=$(get_case_attr "用例名称" "$_tst_case_file")

    mkdir -p "$_tst_log_dir"

    echo "execute $_tst_case_name: $_tst_exec_file $*"
    "$_tst_exec_file" "$@" || _tst_ret=1

    return $_tst_ret
}

# 执行指定列表文件的用例
# $1 -- 要执行的用例列表文件，每行一个用例，井号'#'表示忽略对应用例
tsuite_run_some() {
    if [ ! -f "$1" ]; then
        echo "testcase list file '$1' not exist"
    fi
    local _tst_run_list="$TST_TS_TOPDIR/logs/run.list"
    local _tst_run_result="$TST_TS_TOPDIR/logs/run.result"
    rm -rf "$_tst_run_list" "$_tst_run_result"
    # 去掉注释行和空行，删掉行前的空格
    grep -v "^[[:blank:]]*#" "$1" | grep -v "^[[:blank:]]*$" | sed "s|^[[:blank:]]\+||g" >"$_tst_run_list"

    local _tst_ret=0
    local _tst_nr_pass=0
    local _tst_nr_skip=0
    local _tst_nr_manual=0
    local _tst_nr_fail=0
    local _tst_index=1
    local _tst_nr_all_case
    _tst_nr_all_case=$(wc -l <"$_tst_run_list")
    local _tst_case_time_start
    local _tst_case_time_end
    local _tst_case_cost

    printf "\n%s     -     : %-30s ==>" "$(date '+%Y%m%d-%H%M%S')" "ts_setup"
    _tst_case_time_start=$(get_up_time_ms)
    if tsuite_setup >"$TST_TS_TOPDIR/logs/ts_setup.log" 2>&1; then
        _tst_case_time_end=$(get_up_time_ms)
        _tst_case_cost=$(diff_time_ms2sec "$_tst_case_time_start" "$_tst_case_time_end")
        echo " $(print_green PASS)   (cost $_tst_case_cost)"
    else
        _tst_case_time_end=$(get_up_time_ms)
        _tst_case_cost=$(diff_time_ms2sec "$_tst_case_time_start" "$_tst_case_time_end")
        echo " $(print_red FAIL)   (cost $_tst_case_cost)"
    fi
    mkdir -p "$TST_TS_TOPDIR/logs/testcase"
    for i in $(seq "$_tst_nr_all_case"); do
        local _tst_case_file
        local _tst_case_name
        local _tst_log_name
        local _tst_case_result
        _tst_case_file="$(sed -n "${i}p" "$_tst_run_list")"
        _tst_case_name="$(get_case_attr "用例名称" "$_tst_case_file")"
        _tst_log_name="$(echo "${_tst_case_file#*testcase/}" | tr ' /()[]' '_')"
        local _tst_case_log="$TST_TS_TOPDIR/logs/testcase/${_tst_log_name}.log"
        printf "%s %4d/%-4d : %-30s ==>" "$(date '+%Y%m%d-%H%M%S')" $_tst_index "$_tst_nr_all_case" "$_tst_case_name"
        _tst_case_time_start=$(get_up_time_ms)
        tsuite_run_one "$_tst_case_file" >"$_tst_case_log" 2>&1
        _tst_case_time_end=$(get_up_time_ms)
        _tst_case_cost=$(diff_time_ms2sec "$_tst_case_time_start" "$_tst_case_time_end")
        _tst_case_result=$(grep "RESULT : .* ==> \[  [A-Z]\+  \]" "$_tst_case_log" |
            tail -n 1 | sed "s|.* ==> \[  \([A-Z]\+\)  \].*|\1|g")
        case "$_tst_case_result" in
            "PASSED")
                _tst_nr_pass=$((_tst_nr_pass + 1))
                echo "$_tst_index/$_tst_nr_all_case $_tst_case_name PASS cost:$_tst_case_cost" >>"$_tst_run_result"
                echo " $(print_green PASS)   (cost $_tst_case_cost)"
                ;;
            "SKIP")
                _tst_nr_skip=$((_tst_nr_skip + 1))
                echo "$_tst_index/$_tst_nr_all_case $_tst_case_name SKIP cost:$_tst_case_cost" >>"$_tst_run_result"
                echo " $(print_yellow SKIP)   (cost $_tst_case_cost)"
                ;;
            "MANUAL")
                _tst_nr_manual=$((_tst_nr_manual + 1))
                echo "$_tst_index/$_tst_nr_all_case $_tst_case_name MANUAL cost:$_tst_case_cost" >>"$_tst_run_result"
                echo " $(print_purple MANUAL) (cost $_tst_case_cost)"
                ;;
            *)
                _tst_nr_fail=$((_tst_nr_fail + 1))
                echo "$_tst_index/$_tst_nr_all_case $_tst_case_name FAIL cost:$_tst_case_cost" >>"$_tst_run_result"
                echo " $(print_red FAIL)   (cost $_tst_case_cost)"
                _tst_ret=1
                ;;
        esac
        # 将失败用例执行日志copy到allure_data中
        if ! grep -q "\[  PASSED  \]" "$_tst_case_log"; then
            cp "$_tst_case_log" "$ALLURE_DATA_DIR"
        fi
        _tst_index=$((_tst_index + 1))
    done
    printf "%s     -     : %-30s ==>" "$(date '+%Y%m%d-%H%M%S')" "ts_teardown"
    _tst_case_time_start=$(get_up_time_ms)
    if tsuite_teardown >"$TST_TS_TOPDIR/logs/ts_teardown.log" 2>&1; then
        _tst_case_time_end=$(get_up_time_ms)
        _tst_case_cost=$(diff_time_ms2sec "$_tst_case_time_start" "$_tst_case_time_end")
        echo " $(print_green PASS)   (cost $_tst_case_cost)"
    else
        _tst_case_time_end=$(get_up_time_ms)
        _tst_case_cost=$(diff_time_ms2sec "$_tst_case_time_start" "$_tst_case_time_end")
        echo " $(print_red FAIL)   (cost $_tst_case_cost)"
    fi

    echo ""
    echo " total: $_tst_nr_all_case"
    echo "  $(print_green pass): $_tst_nr_pass"
    echo "  $(print_yellow skip): $_tst_nr_skip"
    echo "$(print_purple manual): $_tst_nr_manual"
    echo "  $(print_red fail): $_tst_nr_fail"
    echo ""

    return $_tst_ret
}

tsuite_run_all() {
    get_all_testcase
    tsuite_run_some "$TST_TS_SYSDIR/all_testcase.list"
}

tsuite_configure() {
    local _tst_ret=0

    echo "configure the testsuite"
    if [ -x "$TST_TS_TOPDIR/configure" ]; then
        if "$TST_TS_TOPDIR/configure"; then
            echo "testsuite configure execute success"
        else
            echo "testsuite configure execute fail"
            _tst_ret=1
        fi
    else
        echo "configure file not exist or executable"
    fi
    if [ "$TST_TS_TOPDIR" != "$TST_COMMON_TOPDIR" ]; then
        if [ -x "$TST_COMMON_TOPDIR/configure" ]; then
            if "$TST_COMMON_TOPDIR/configure"; then
                echo "tsuite common configure execute success"
            else
                echo "tsuite common configure execute fail"
                _tst_ret=1
            fi
        fi
    fi

    if [ $_tst_ret -eq 0 ]; then
        echo "configure success"
    else
        echo "configure fail"
    fi
    return $_tst_ret
}

tsuite_compile() {
    local _tst_ret=0

    echo "try cleanall before compile"
    make -k -C "$TST_TS_TOPDIR" cleanall || _tst_ret=1
    tsuite_configure || _tst_ret=1
    echo "compile the testsuite"
    make -k -C "$TST_TS_TOPDIR" all || _tst_ret=1

    if [ $_tst_ret -eq 0 ]; then
        echo 0 >"$TST_TS_SYSDIR/compile.stat"
        echo "compile success"
    else
        echo 1 >"$TST_TS_SYSDIR/compile.stat"
        echo "compile fail"
    fi
    return $_tst_ret
}

tsuite_setup() {
    local _tst_ret=0

    echo "tsuite try execute ts_setup"
    if [ -f "$TST_TS_TOPDIR/tst_common/lib/tst_ts_setup" ]; then
        "$TST_TS_TOPDIR/tst_common/lib/tst_ts_setup" || _tst_ret=1
    elif [ -f "$TST_TS_TOPDIR/lib/tst_ts_setup" ]; then
        # 只是tst_suite_common使用
        "$TST_TS_TOPDIR/lib/tst_ts_setup" || _tst_ret=1
    else
        echo "the tst_ts_setup not found"
        _tst_ret=1
    fi

    return $_tst_ret
}

tsuite_run() {
    local _tst_ret=0

    # 生成结果记录文件
    _get_tst_result_file

    # 生成allure_data目录
    _get_allure_data_dir

    if [ -z "$1" ]; then
        tsuite_run_all || _tst_ret=1
    elif [ "$1" == "-f" ]; then
        echo "Try run testcases in list file '$2'"
        tsuite_run_some "$2" || _tst_ret=1
    elif [ "$1" == "-l" ]; then
        echo "Try run testcases with level '$2'"
        rm -rf "$TST_TS_SYSDIR/level.list"
        for l in $(echo "$2" | tr ',' ' '); do
            grep -r "@用例级别:[[:blank:]]*$l" ./testcase | awk -F : '{print $1}' |
                sort | uniq >>"$TST_TS_SYSDIR/level.list"
        done
        tsuite_run_some "$TST_TS_SYSDIR/level.list" || _tst_ret=1
    else
        local _a_log
        _a_log="$(echo "${@##*/}" | tr ' /()[]' '_')"
        _a_log="${_a_log//.test/.c}"
        tsuite_run_one "$@" 2>&1 | tee "$ALLURE_DATA_DIR/$_a_log.log"
        if [ ${PIPESTATUS[0]} -ne 0 ]; then
            _tst_ret=1
        fi
    fi

    sed -i "/^suite-end-time:/c suite-end-time: $(get_timestamp_ms)" "$TST_RESULT_FILE"

    #    local datatime=$(date '+%Y%m%d%H%M%S')
    #    if [ -f /etc/tst-env.conf ] && [ "$(ls -A "$ALLURE_DATA_DIR"/*.json 2>/dev/null)" ]; then
    #    if [ "$(ls -A "$ALLURE_DATA_DIR"/*.json 2>/dev/null)" ]; then
    #      # 下载allure2包
    #      if [ ! -d "$TST_TS_TOPDIR/tools/allure-2.29.0" ]; then
    #        wget -q -P "$TST_TS_TOPDIR/tools" -N http://tst.tlinux.woa.com/file_repo/tools/allure2_tools/allure-2.29.0.tar.gz
    #        tar -xf "$TST_TS_TOPDIR/tools/allure-2.29.0.tar.gz" -C "$TST_TS_TOPDIR/tools"
    #      fi
    # 生成allure报告
    #      "$TST_TS_TOPDIR"/tools/allure-2.29.0/bin/allure generate --lang zh logs/allure_data --output logs/allure_report/"$datatime" --clean > /dev/null
    # 上传报告到文件服务器
    #      scp -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -r logs/allure_report/"$datatime" root@9.134.184.38:/repo/logs/allure_report/
    #    fi

    # 判断tcase $TCASE_TASK_ID和$TCASE_MATER_RECORD_ID是否存在
    if [ -n "$TCASE_TASK_ID" ] && [ -n "$TCASE_MASTER_RECORD_ID" ]; then
        file_server_data_save_path="${TCASE_TASK_ID}_${TCASE_MASTER_RECORD_ID}"
    else
        file_server_data_save_path=${TST_RUN_DATATIME}
    fi

    # 如果想将报告数据上传，则需要增加$ALLURE_REPORT环境变量
    if [ "$ALLURE_REPORT" = "TCASE" ]; then
        # 如果allure_data中包含json文件，则上传
        if ls "$ALLURE_DATA_DIR"/*.json 1>/dev/null 2>&1; then
            for file in "$ALLURE_DATA_DIR"/*; do
                if [ -f "$file" ]; then
                    curl -X POST -F "file=@$file" \
                        -F "project=file_repo" \
                        -F "key=others/case_report/allure_data/$file_server_data_save_path" \
                        http://tst.tlinux.woa.com:55588/server/upload/UploadFile >/dev/null 2>&1
                fi
            done
        fi
    fi

    return $_tst_ret
}

tsuite_teardown() {
    local _tst_ret=0

    mkdir -p "$TST_TS_TOPDIR/logs"
    echo "tsuite try execute ts_teardown"
    if [ -f "$TST_TS_TOPDIR/tst_common/lib/tst_ts_teardown" ]; then
        "$TST_TS_TOPDIR/tst_common/lib/tst_ts_teardown" || _tst_ret=1
    elif [ -f "$TST_TS_TOPDIR/lib/tst_ts_teardown" ]; then
        # 只是tst_suite_common使用
        "$TST_TS_TOPDIR/lib/tst_ts_teardown" || _tst_ret=1
    else
        echo "the tst_ts_setup not found"
        _tst_ret=1
    fi
    return $_tst_ret
}

tsuite_clean() {
    local _tst_ret=0

    make -k -C "$TST_TS_TOPDIR" clean || _tst_ret=1
    rm -f "$TST_TS_SYSDIR/compile.stat"

    return $_tst_ret
}

tsuite_cleanall() {
    local _tst_ret=0

    make -k -C "$TST_TS_TOPDIR" cleanall || _tst_ret=1
    rm -rfv "$TST_TS_TOPDIR/logs" "$TST_TS_TOPDIR/compile_commands.json"
    rm -f "$TST_TS_SYSDIR/compile.stat"

    return $_tst_ret
}

# 新建用例模板
# $1 -- 用例代码类型：sh/c/py
# $2 -- 用例名
# $3 -- 可选参数，用例模板
tsuite_new_case() {
    local _tst_tc_type="$1"
    local _tst_tc_path="$2"
    local _tst_tc_name
    local _tst_input_template="$3"
    local _tst_tc_id
    _tst_tc_id=$(date '+%Y%m%d-%H%M%S-%N')
    local _tst_tc_file="testcase/${_tst_tc_path}.${_tst_tc_type}"
    local _tst_tc_dir
    local _tst_template_path="$TST_TS_TOPDIR/tst_common/testcase"
    # 为了适配tst_suite_common
    [ -d "$_tst_template_path" ] || _tst_template_path="$TST_TS_TOPDIR/testcase"
    if [ ! -d "$_tst_template_path" ]; then
        echo "the template path is $_tst_template_path, not dir"
        return 1
    fi

    if [ -z "$_tst_tc_path" ]; then
        echo "the testcase name not set"
        return 1
    fi
    _tst_tc_dir="$(dirname "$_tst_tc_file")"
    mkdir -p "$_tst_tc_dir"
    _tst_tc_name="$(basename "$_tst_tc_path")"

    if [ -z "$_tst_input_template" ]; then
        case "$_tst_tc_type" in
            sh)
                cp -v "$_tst_template_path/test_shell_testcase.sh" "$TST_TS_TOPDIR/$_tst_tc_file"
                sed -i "s|}/lib/common|}/tst_common/lib/common|g" "$TST_TS_TOPDIR/$_tst_tc_file"
                ;;
            c)
                cp -v "$_tst_template_path/test_c_testcase.c" "$TST_TS_TOPDIR/$_tst_tc_file"
                ;;
            py)
                cp -v "$_tst_template_path/test_python_testcase.py" "$TST_TS_TOPDIR/$_tst_tc_file"
                if [ -f ./tst_lib/ts_common.py ]; then
                    sed -i "/from lib.common import TestCase/c from tst_lib.ts_common import MyTestCase" \
                        "$TST_TS_TOPDIR/$_tst_tc_file"
                    sed -i "/class PythonTestCase(TestCase):/c class PythonTestCase(MyTestCase):" \
                        "$TST_TS_TOPDIR/$_tst_tc_file"
                fi
                ;;
            *)
                echo "unsupported testcase type"
                return 1
                ;;
        esac
    else
        if [ ! -f "$_tst_input_template" ]; then
            echo "the template file $_tst_input_template not exist"
            return 1
        fi
        if ! echo "$_tst_input_template" | grep "\.$_tst_tc_type$" >/dev/null; then
            echo "the template file $_tst_input_template not match the testcase type $_tst_tc_type"
            return 1
        fi
        cp -v "$_tst_input_template" "$TST_TS_TOPDIR/$_tst_tc_file"
    fi

    sed -i "s|@用例ID:.*|@用例ID: $_tst_tc_id|g" "$TST_TS_TOPDIR/$_tst_tc_file"
    sed -i "s|@用例名称:.*|@用例名称: $_tst_tc_name|g" "$TST_TS_TOPDIR/$_tst_tc_file"
    echo ""
    echo "the new testcase info:"
    echo "  name: $_tst_tc_name"
    echo "  type: $_tst_tc_type"
    echo "    id: $_tst_tc_id"
    echo "  file: $_tst_tc_file"
    echo ""
    return 0
}

tsuite_stress() {
    msg "tsuite stress $*"
    stress_main "$@"
    return $?
}

tsuite_export() {
    python3 "$TST_COMMON_TOPDIR/lib/tsuite_export.py" "$@"
}

tsuite_main() {
    local _tst_ret=0
    local _tst_cmd="$0 $*"
    local _tst_suite_time_start
    _tst_suite_time_start=$(get_up_time_ms)
    local _tst_suite_time_end

    echo "TST_TS_TOPDIR=$TST_TS_TOPDIR"
    cd "$TST_TS_TOPDIR" || return 1
    mkdir -p "$TST_TS_SYSDIR" || return 1

    case "$1" in
        help | -h | --help)
            tsuite_usage
            return 0
            ;;
        new)
            shift
            tsuite_new_case "$@" || _tst_ret=1
            ;;
        list)
            tsuite_list || _tst_ret=1
            ;;
        config)
            tsuite_configure || _tst_ret=1
            ;;
        compile)
            tsuite_compile || _tst_ret=1
            ;;
        sign)
            shift
            main_sign_ko "$@" || _tst_ret=1
            ;;
        setup)
            tsuite_setup || _tst_ret=1
            ;;
        run)
            shift
            tsuite_run "$@" || _tst_ret=1
            ;;
        stress)
            shift
            tsuite_stress "$@" || _tst_ret=1
            ;;
        teardown)
            tsuite_teardown || _tst_ret=1
            ;;
        clean)
            tsuite_clean || _tst_ret=1
            ;;
        cleanall)
            tsuite_cleanall || _tst_ret=1
            ;;
        export)
            shift
            tsuite_export "$@" || _tst_ret=1
            ;;
        *)
            echo "$0 $*"
            tsuite_usage
            return 1
            ;;
    esac

    _tst_suite_time_end=$(get_up_time_ms)
    if [ $_tst_ret -eq 0 ]; then
        echo "execute $_tst_cmd success, cost $(diff_time_ms2sec "$_tst_suite_time_start" "$_tst_suite_time_end")"
    else
        echo "execute $_tst_cmd fail, cost $(diff_time_ms2sec "$_tst_suite_time_start" "$_tst_suite_time_end")"
    fi
    return $_tst_ret
}
